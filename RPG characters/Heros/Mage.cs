﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Hero
{
    class Mage : Hero
    {
        public Mage(string name) : base(name)
        {
            Name = name;
            Strength = 1;
            Dexterity = 1;
            Intelligence = 8;
        }
        public override void LevelUp()
        {
            base.LevelUp();
            Strength += 1;
            Dexterity += 1;
            Intelligence += 5;
        }
    }
}
