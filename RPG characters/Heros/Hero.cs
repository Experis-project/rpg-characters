﻿using RPG_characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPG_characters.Items.Item;

namespace RPG_characters.Hero
{
    abstract class Hero
    {
        public string Name { get; protected set; }
        public int Level { get; set; } = 1;
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public Dictionary<ItemSlot, Item> Items { get; set; } = new Dictionary<ItemSlot, Item>();
        public Hero(string name)
        {
            Name = name;
            Level = 1;
        }

        // LevelUp method adds one level to the hero object
        public virtual void LevelUp()
        {
            Level++;

        }
    }
}
