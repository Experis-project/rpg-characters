# RPG characters

RPG characters is a CLI app created to practice C#. It lets user create a fantasy character, level up, create weapons & armor and equipping it. You can also see your characters stats.

## Add your files

## Features
- Create a character with class and name
- See your characters strength, dexterity and Intelligence
- Fight monsters to level up. Characters attributes increase with each level.
- Create weapons and armor of different types. Weapons have randomly generated DPS level requirement.

## Running the app
```
- You may need an IDE like Visual Studio
- Clone the project to your computer
- Open project in your IDE and press run button
```
## Usage
After starting the app in a terminal, you will get instructions on what you can do. All available commands will be given at each stage.

## License
[MIT](https://choosealicense.com/licenses/mit/)
