﻿using RPG_characters.Hero;
using RPG_characters.Items;
using System.ComponentModel.Design;

// Start by giving hero type and name you want to create
Console.WriteLine("Create a warrior, mage, ranger or rogue. Use small letters: ");
string heroClass = Console.ReadLine();
Console.WriteLine($"\nGreat! And what is {heroClass}'s name?");
string heroName = Console.ReadLine();
Console.Clear();
Hero hero;

// Weapon creation asks for type and name. Then equips it to a weapon slot. 
void createWeapon()
{
    Console.Clear();
    Console.WriteLine("\nChoose from the following (Capital first letter):\n Axe\n Bow\r\n Dagger\r\n Hammer\r\n Staff\r\n Sword\r\n Wand");
    string weaponType = Console.ReadLine();
    Console.WriteLine("What name do you want to give it?");
    string weaponName = Console.ReadLine();
    if (Enum.TryParse(weaponType, out Weapon.Type thisType))
    {
        Weapon weapon = new Weapon(weaponName, thisType);
        hero.Items.Add(Item.ItemSlot.Weapon, weapon);
        Console.WriteLine($"{hero.Name} equipped {hero.Items[Item.ItemSlot.Weapon].Name}");
    }
    else
    {
        Console.WriteLine("No such weapon type.");
    }
}


// Armor creation asks for type and name. Then equips it to non-weapon slot. 
void createArmor()
{
    Console.Clear();
    Console.WriteLine("\nChoose from the following (Capital first letter):\n Cloth\n Leather\r\n Mail\r\n Plate");
    string armorType = Console.ReadLine();
    Console.WriteLine("What name do you want to give it?");
    string armorName = Console.ReadLine();
    if (Enum.TryParse(armorType, out Armor.Type thisType))
    {
        Armor armor = new Armor(armorName, thisType);
        hero.Items.Add(Item.ItemSlot.Head, armor);
        Console.WriteLine($"{hero.Name} equipped {hero.Items[Item.ItemSlot.Head].Name}");
    }
    else
    {
        Console.WriteLine("No such armor type.");
    }
}

// Main loop of program waits for commands from user, then calls appropriate methods and functions. 
void mainLoop(Hero hero)
{
    Console.WriteLine($"\nCreated a {heroClass} called {heroName}!\nAvailable commands:\n stats\n fight\n weapon\n armor");

    while (true)
    {
        var command = Console.ReadLine();
        if (command == "stats") 
        {
            Console.WriteLine(value: $"\n{heroName} is a level {hero.Level} {heroClass}\nStrength: {hero.Strength} \nDexterity: {hero.Dexterity} \nIntelligence: {hero.Intelligence}");
        } 
        else if (command == "fight") 
        {
            hero.LevelUp();
            Console.WriteLine($"\nYou fought bravely and reached level {hero.Level}");
        }
        else if (command == "weapon") { createWeapon(); }
        else if (command == "armor")  { createArmor(); }
        else 
        {
            Console.WriteLine("\nAvailable commands:\n stats\n fight\n weapon\n armor");
        }
    }
}

// Here program creates a hero object based on type and name the user entered. Then launches main loop of program that waits for commands. 
switch (heroClass)
{
    case "warrior":
        Warrior warrior = new Warrior(heroName);
        hero = warrior;
        mainLoop(hero);
        break;
    case "mage":
        Mage mage = new Mage(heroName);
        hero = mage;
        mainLoop(hero);
        break;
    case "ranger":
        Ranger ranger = new Ranger(heroName);
        hero = ranger;
        mainLoop(hero);
        break;       
    case "rogue":
        Rogue rogue = new Rogue(heroName);
        hero = rogue;
        mainLoop(hero);
        break;
    default:
        Console.WriteLine("No such hero. Start again.");
        break;
}
