﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Items
{
    internal class Weapon : Item
    {
        public int BaseDamage { get; set; }
        public int AttacksPerSecond { get; set; }
        public int Dps { get; set; }
        public enum Type
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }
        public Type WeaponType { get; set; }
        public Weapon(string name, Type weaponType) : base(name)
        {
            Name = name;

            BaseDamage = rnd.Next(1, 100);
            AttacksPerSecond = rnd.Next(1, 11);
            Dps = BaseDamage * AttacksPerSecond;
            WeaponType = weaponType;
            Console.WriteLine($"Created {weaponType} called {Name}. It has DPS of {Dps} and requires level {LevelRequired}.");
        }
    }
}
