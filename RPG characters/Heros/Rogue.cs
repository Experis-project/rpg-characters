﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Hero
{
    class Rogue : Hero
    {
        public Rogue(string name) : base(name)
        {
            Name = name;
            Strength = 2;
            Dexterity = 6;
            Intelligence = 1;
        }
        public override void LevelUp()
        {
            base.LevelUp();
            Strength += 1;
            Dexterity += 4;
            Intelligence += 1;
        }
    }
}
