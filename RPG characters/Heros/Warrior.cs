﻿using RPG_characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Hero
{
    class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {
            Name = name;
            Strength = 5;
            Dexterity = 2;
            Intelligence = 1;
        }
        public override void LevelUp()
        {
            base.LevelUp();
            Strength += 3;
            Dexterity += 2;
            Intelligence += 1;
        }
    }
}
