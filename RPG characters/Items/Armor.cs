﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Items
{
    internal class Armor : Item
    {
        public enum Type
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
        public Type ArmorType { get; set; }

        public Armor(string name, Type armorType) : base(name)
        {
            Name = name;
            ArmorType = armorType;
            Console.WriteLine($" {name} of type {armorType} created. Requires level {LevelRequired}");
        }
    }
}
