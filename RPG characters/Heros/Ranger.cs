﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RPG_characters.Hero
{
    internal class Ranger : Hero
    {
        public Ranger(string name) : base(name)
        {
            Name = name;
            Strength = 1;
            Dexterity = 7;
            Intelligence = 1;
        }
        public override void LevelUp()
        {
            base.LevelUp();
            Strength += 3;
            Dexterity += 2;
            Intelligence += 1;
        }
    }
}
