﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Items
{
    abstract class Item
    {
        public string Name { get; set; }
        public int LevelRequired { get; set; }

        public Random rnd = new Random();
        public Item(string name)
        {
            Name = name;
            LevelRequired = rnd.Next(1, 11);
        }
        public enum ItemSlot
        {
            Head,
            Body,
            Legs,
            Weapon
        }
        public ItemSlot Slot { get; set; }
    }
}
